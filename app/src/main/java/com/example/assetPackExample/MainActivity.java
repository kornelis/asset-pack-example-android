package com.example.assetPackExample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.play.core.assetpacks.AssetPackLocation;
import com.google.android.play.core.assetpacks.AssetPackManager;
import com.google.android.play.core.assetpacks.AssetPackManagerFactory;
import com.google.android.play.core.assetpacks.AssetPackState;
import com.google.android.play.core.assetpacks.AssetPackStateUpdateListener;
import com.google.android.play.core.assetpacks.AssetPackStates;
import com.google.android.play.core.tasks.OnCompleteListener;
import com.google.android.play.core.tasks.Task;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author changcsw
 *
 * Using an Activity to do the work.
 * Keeping it in one place to show how stuff should work.
 *
 * All results are put into the Log, with production code this should be replaced with your own code.
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    AssetPackManager mAssetPackManager;
    AssetPackStateUpdateListener mAssetPackStateUpdateListener;

    final String TAG_FILE_CONTENTS = "file contents";
    final String TAG_ASSET_PACK = "asset pack";

    final String assetPackInstallTime = "asset_pack_install_time";
    final String assetPackFastFollow = "asset_pack_fast_follow";
    final String assetPackNameOnDemand = "asset_pack_on_demand";

    final String assetFileApp = "asset_file_app";
    final String assetFileInstallTime = "asset_file_install_time";
    final String assetFileFastFollow = "asset_file_fast_follow";
    final String assetFileOnDemand = "asset_file_on_demand";
    final String textFile = ".txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        findViewById(R.id.text).setOnClickListener(this);

        loadInstallTimeAssets();

        mAssetPackStateUpdateListener = initAssetPackStateUpdateListener();
        mAssetPackManager = initAssetPackManger(assetFileFastFollow, mAssetPackStateUpdateListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAssetPackManager.unregisterListener(mAssetPackStateUpdateListener);
    }

    @Override
    public void onClick(View v) {
        getAbsoluteAssetPath(assetPackFastFollow, "/fast_follow/" + assetFileFastFollow + "_one" + textFile);
        getAbsoluteAssetPath(assetPackNameOnDemand, "/" + assetFileOnDemand + "_two" + textFile);
    }

    /**
     * Install-time can be used directly by AssetManager to access, the same as the Assets directory in the main project.
     */
    private void loadInstallTimeAssets() {
        AssetManager assetManager = this.getAssets();
        try {
            InputStream is = assetManager.open(assetFileInstallTime + "_one" + textFile);
            int length = is.available();
            byte[] buffer = new byte[length];
            is.read(buffer);

            Log.d(TAG_FILE_CONTENTS, new String(buffer));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return AssetPackStateUpdateListener
     */
    private AssetPackStateUpdateListener initAssetPackStateUpdateListener() {
        return new AssetPackStateUpdateListener() {
            @Override
            public void onStateUpdate(AssetPackState state) {
                Log.d(TAG_ASSET_PACK, "mAssetPackStateUpdateListener onStateUpdate state: " + state.status());
            }
        };
    }

    /**
     * Create the AssetPackManager.
     * And get the states of the selected asset_pack.
     * Add an addOnCompleteListener() and log the result.
     *
     * @param assetPackName the name of the asset_pack we want update for.
     * @param updateListener if update in the state of the asset pack, we use the listener to do things with the update.
     * @return AssetPackManager
     */
    private AssetPackManager initAssetPackManger(final String assetPackName, AssetPackStateUpdateListener updateListener) {
        AssetPackManager manager = AssetPackManagerFactory.getInstance(this.getApplicationContext());
        manager.getPackStates(Collections.singletonList(assetPackName))
                .addOnCompleteListener(new OnCompleteListener<AssetPackStates>() {
                    @Override
                    public void onComplete(Task<AssetPackStates> task) {
                        AssetPackStates assetPackStates;
                        try {
                            assetPackStates = task.getResult();
                            AssetPackState assetPackState = assetPackStates.packStates().get(assetPackName);

                            Log.d(TAG_ASSET_PACK, "status: " + assetPackState.status() +
                                    ", name: " + assetPackState.name() +
                                    ", errorCode: " + assetPackState.errorCode() +
                                    ", bytesDownloaded: " + assetPackState.bytesDownloaded() +
                                    ", totalBytesToDownload: " + assetPackState.totalBytesToDownload() +
                                    ", transferProgressPercentage: " + assetPackState.transferProgressPercentage());
                        } catch (Exception e){
                            Log.d("MainActivity", e.getMessage());
                        }
                    }
                });

        manager.registerListener(updateListener);

        List<String> list = new ArrayList();
        list.add(assetPackFastFollow);
        list.add(assetPackNameOnDemand);
        manager.fetch(list);

        return manager;
    }

    /**
     * Fast-follow with on-demand needs to get the path of asset_pack through AssetPackLocation, and read the resource according to the absolute path.
     *
     * @param assetPack name of the asset_pack
     * @param relativeAssetPath for the assets, could contain the folder the asset is located in.
     */
    private void getAbsoluteAssetPath(String assetPack, String relativeAssetPath) {
        AssetPackLocation assetPackPath = mAssetPackManager.getPackLocation(assetPack);
        Log.d(TAG_ASSET_PACK, "invoke getAbsoluteAssetPath");

        String assetsFolderPath = assetPackPath.assetsPath();
        // equivalent to: FilenameUtils.concat(assetPackPath.path(), "assets");

        File file = new File(assetsFolderPath + relativeAssetPath);
        if (file.isFile()) {
            try {
                InputStream is = new FileInputStream(file);
                int length = is.available();
                byte[] buffer = new byte[length];
                is.read(buffer);
                Log.d(TAG_FILE_CONTENTS, new String(buffer));
            }
            catch (Exception e) {
               e.printStackTrace();
            }
        } else {
            Log.d("File not found", assetPack + " " + relativeAssetPath);
        }
    }
}

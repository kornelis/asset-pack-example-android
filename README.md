# Asset Pack Example for Android

## Configuration of project

### Create Asset Pack

1. Create a resource package folder in the root directory of the project (under the same directory as the App module), and the folder name is the resource package name. Note: The name can only be opened with letters, and can only contain **letters, numbers and underscores**, such as ：`base_assets`
2. In `base_assets` Create under directory: `src/main/assets`, store our resource files in this directory, there can be subdirectories in this directory.
3. In `base_asssets`Create under directory `build.gradle`File and fill in the content as follows:

```groovy
// In the asset pack’s build.gradle file:
apply plugin: 'com.android.asset-pack'

assetPack {
    packName = "base_assets" // Directory name for the asset pack
    dynamicDelivery {
        deliveryType = "install-time" // According to your needs, you can fill in one of the following three types: [ install-time | fast-follow | on-demand ]
    }
}
```

4. In the App (Module: app) module `build.gradle` Add the following code:

```groovy
// In the app build.gradle file:
android {
    ...
    assetPacks = [":base_assets"]
}
```

5. In the root directory of the project there is a `settings.gradle` file, add your own resource pack to this file as follows:

```groovy
// In the settings.gradle file:
include ':app'
include ':base_assets'
```


### Using an Asset Pack

1. Under the app (Module: app) module of the project `build.gradle` add the needed libary: `Google Play Core`  library is as follows:

```groovy
// In your app’s build.gradle file:
...
dependencies {
    // This dependency is downloaded from the Google’s Maven repository.
    // So, make sure you also include that repository in your project's build.gradle file.
    implementation 'com.google.android.play:core:1.7.3'
    ...
}
```

2. Get the resources in the resource pack:

   - Install-time The type of resource pack is used just like the resources in the assets directory in the App (Module: app) module

   - Fast-follow Type of resource pack will be downloaded immediately after the application is installed.

   - On-demand Type of resource pack can be downloaded on demand.

     In use fast-follow with on-demand The type of resource package needs to find the resource package path through the following method, and then read the resource (reading first check whether the local download is complete, check the process reference: https://developer.android.com/guide/playcore/asset-delivery/integrate-java）.

    ```java
    AssetPackLocation assetPackPath = assetPackManager.getPackLocation(assetPack);
    String assetsFolderPath = assetPackPath.assetsPath(); // This directory is the resource package directory.
    ```

### Generate an Android App Bundle

1. With Android Studio: Build -> generate Signed Bundle/APK -> Step by step export generation Android App Bundle，which is the `.aab` type file.

### Testing localy.

1. Generate apks file with test label:

   ```shell
   java -jar bundletool-all.jar build-apks --bundle=path/to/your/bundle.aab \
     --output=output.apks --local-testing
   ```

2. Connect the device and install the apks file through bundletool:

   ```shell
   java -jar bundletool.jar install-apks --apks=output.apks
   ```

3. Open the installed apks and check whether the resources can be obtained normally

### Testing with Android Play Store.

1. Upload signed bundle to the Google Play Store, preferably internal testing.
2. Check that you are among the internal testers in the list, if not, add yourself.
3. Open up Play Store on your device -> My Apps & games -> Update (or Update All).
4. Attach device to Android Studio to view the logs.


## Licence

The original author of this project, changcsw, did not include a Licence. I propose that this project goes with the MIT licence.

## Credits
 - [PADSameple-Android](https://github.com/changcsw/PADSameple-Android):  [changcsw](https://github.com/changcsw)
 - Translation to English: Kornelis Marijs.
